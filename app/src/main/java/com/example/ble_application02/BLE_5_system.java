package com.example.ble_application02;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertisingSet;
import android.bluetooth.le.AdvertisingSetCallback;
import android.bluetooth.le.AdvertisingSetParameters;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.ParcelUuid;

import java.util.UUID;

//Class is used when BLE 5 is supported
public class BLE_5_system {
    private static final String BLE_UUID = "CDB7950D-73F1-4D4D-8E47-C090502DBD63";

    private BluetoothLeAdvertiser mBle5_advertiser;
    private AdvertisingSetParameters parameters;
    private AdvertiseData data;
    private MainActivity ma;
    private String mData = "1";
    private boolean isScanning = false;
    private int SIGNAL_STRENGTH;
    private int SIGNAL_INTERVAL;

    public BLE_5_system(BluetoothAdapter adapter, MainActivity main){
        mBle5_advertiser = adapter.getBluetoothLeAdvertiser();
        ma = main;
    }

    public void ble5Advertise(){
        parameters = null;
        data = null;
        byte[] info = mData.getBytes();
        parameters = (new AdvertisingSetParameters.Builder())
                .setLegacyMode(true)
                .setConnectable(false)
                .setScannable(false)
                .setInterval(SIGNAL_INTERVAL)
                .setTxPowerLevel(SIGNAL_STRENGTH)
                .build();
        ParcelUuid pUuid = new ParcelUuid(UUID.fromString(BLE_UUID));
        data = (new AdvertiseData.Builder())
                .setIncludeDeviceName(true)
                .setIncludeTxPowerLevel(true)
                //.addServiceData(pUuid, info)
                .build();
    }

    //Sets or changes the signal strength
    public void setSignalStrengthBLE5(int signalStrength){
        SIGNAL_STRENGTH = signalStrength;
        /*switch (signalStrength){
            case 1:
                SIGNAL_STRENGTH = -127;
                ma.device_signalStrengthLevel.setText("Signal Strength: MIN");
                break;
            case 2:
                SIGNAL_STRENGTH = -21;
                ma.device_signalStrengthLevel.setText("Signal Strength: ULTRA LOW");
                break;
            case 3:
                SIGNAL_STRENGTH = -15;
                ma.device_signalStrengthLevel.setText("Signal Strength: LOW");
                break;
            case 4:
                SIGNAL_STRENGTH = -7;
                ma.device_signalStrengthLevel.setText("Signal Strength: MEDIUM");
                break;
            case 5:
                SIGNAL_STRENGTH = 1;
                ma.device_signalStrengthLevel.setText("Signal Strength: MAX");
                break;
        }*/
    }

    //Sets or changes the signal interval
    public void setSignalIntervalBLE5(int signalInterval){
        SIGNAL_INTERVAL = signalInterval;
        /*switch (signalInterval){
            case 1:
                SIGNAL_INTERVAL = 160;
                ma.device_signalIntervalLevel.setText("Signal Interval: LOW");
                break;
            case 2:
                SIGNAL_INTERVAL = 400;
                ma.device_signalIntervalLevel.setText("Signal Interval: MEDIUM");
                break;
            case 3:
                SIGNAL_INTERVAL = 1600;
                ma.device_signalIntervalLevel.setText("Signal Interval: HIGH");
                break;
            case 4:
                SIGNAL_INTERVAL = 16777215;
                ma.device_signalIntervalLevel.setText("Signal Interval: MAX");
                break;
        }*/
    }
    public void ble5start(){
        if(!isScanning){
            ble5Advertise();
            mBle5_advertiser.startAdvertisingSet(parameters, data, null, null, null, mBle5Callback);
            isScanning = true;
        }
    }

    public void ble5stop(){
        mBle5_advertiser.stopAdvertisingSet(mBle5Callback);
        isScanning = false;
    }

    AdvertisingSetCallback mBle5Callback = new AdvertisingSetCallback() {
        @Override
        public void onAdvertisingSetStarted(AdvertisingSet advertisingSet, int txPower, int status) {
            ma.device_status.setText("Advertising");
            ma.device_data.setText(data.toString());
            ma.device_settings.setText(parameters.toString());
            super.onAdvertisingSetStarted(advertisingSet, txPower, status);
        }

        @Override
        public void onAdvertisingSetStopped(AdvertisingSet advertisingSet) {
            super.onAdvertisingSetStopped(advertisingSet);
        }

        @Override
        public void onAdvertisingDataSet(AdvertisingSet advertisingSet, int status) {
            super.onAdvertisingDataSet(advertisingSet, status);
        }

        @Override
        public void onScanResponseDataSet(AdvertisingSet advertisingSet, int status) {
            super.onScanResponseDataSet(advertisingSet, status);
        }

        @Override
        public void onAdvertisingParametersUpdated(AdvertisingSet advertisingSet, int txPower, int status) {
            super.onAdvertisingParametersUpdated(advertisingSet, txPower, status);
        }
    };
}

