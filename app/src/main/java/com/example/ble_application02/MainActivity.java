package com.example.ble_application02;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.le.AdvertiseCallback;
import android.bluetooth.le.AdvertiseData;
import android.bluetooth.le.AdvertiseSettings;
import android.bluetooth.le.BluetoothLeAdvertiser;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelUuid;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    private BluetoothLeAdvertiser mBLE_advertiser;
    private AdvertiseSettings mAdvertiserSettings;
    private AdvertiseData mBleAdvertiseData;
    private BluetoothAdapter mBleAdapter;
    private BLE_5_system mBle5;

    private Button onButton,offButton, signalStrength_increase, signalStrength_Decrease, signalInterval_Increase, signalInterval_Decrease;
    public TextView device_status, device_data, device_settings, device_mode, device_signalStrengthLevel, device_signalIntervalLevel;

    private ParcelUuid pUuid;

    private static final String BLE_UUID = "CDB7950D-73F1-4D4D-8E47-C090502DBD63";
    private boolean isBle5Compatible = false;
    private int SIGNAL_STRENGTH = -6;
    private int SIGNAL_INTERVAL = 1600;
    private int signalIntervalCounter = 3;

    public MainActivity() {

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mBleAdapter = BluetoothAdapter.getDefaultAdapter();
        if( !mBleAdapter.isMultipleAdvertisementSupported() ) {
            Toast.makeText( this, "Multiple advertisement not supported", Toast.LENGTH_SHORT ).show();
            //mAdvertiseButton.setEnabled( false );
            //mDiscoverButton.setEnabled( false );
        }
        findViewByIdes();       //Initiates the gui
        //Runs if we can use BLE 5
        if(checkBLE5Compatibility()){
            mBle5 = new BLE_5_system(mBleAdapter, this);
            isBle5Compatible = true;
            device_mode.setText("Mode: BLE 5");
            mBle5.setSignalStrengthBLE5(SIGNAL_STRENGTH);
            mBle5.setSignalIntervalBLE5(SIGNAL_INTERVAL);
        }
        //Runs if we can't use BLE 5
        else{
            try {
                advertise();
                device_mode.setText("Mode: BLE");
            } catch (UnsupportedEncodingException e) {
                Toast.makeText(this, "Advertising failed", Toast.LENGTH_SHORT).show();
                device_mode.setText("Mode: Fail");
            }
        }
        setSignalInterval(signalIntervalCounter); //Sets and writes the signal interval
        device_signalStrengthLevel.setText("Signal Strength: " + SIGNAL_STRENGTH + "dBm"); //Writes out the signal level
        implementListeners();   //Sets up the listeners for the gui
    }

    //Sets up the listeners for the gui
    private void implementListeners() {
        onButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isBle5Compatible){
                    mBle5.ble5start();
                }
                else{
                    mBLE_advertiser.startAdvertising(mAdvertiserSettings, mBleAdvertiseData, mBleAdvCallback);
                }
            }
        });

        offButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isBle5Compatible){
                    mBle5.ble5stop();
                }
                else{
                    mBLE_advertiser.stopAdvertising(mBleAdvCallback);
                }
                device_status.setText("Status");
                device_data.setText("Data");
                device_settings.setText("Settings");
            }
        });
        signalInterval_Increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (signalIntervalCounter < 4) {
                    signalIntervalCounter++;
                    setSignalInterval(signalIntervalCounter);
                    if(isBle5Compatible){
                        mBle5.setSignalIntervalBLE5(SIGNAL_INTERVAL);
                    }
                }
            }
        });
        signalInterval_Decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(signalIntervalCounter > 1){
                    signalIntervalCounter--;
                    setSignalInterval(signalIntervalCounter);
                    if(isBle5Compatible){
                        mBle5.setSignalIntervalBLE5(SIGNAL_INTERVAL);
                    }
                }
            }
        });
        signalStrength_increase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SIGNAL_STRENGTH < 0) {
                    SIGNAL_STRENGTH++;
                    if(isBle5Compatible){
                        mBle5.setSignalStrengthBLE5(SIGNAL_STRENGTH);
                    }
                }
                device_signalStrengthLevel.setText("Signal Strength: " + SIGNAL_STRENGTH + "dBm");
            }
        });
        signalStrength_Decrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SIGNAL_STRENGTH > -127){
                    SIGNAL_STRENGTH--;
                    if(isBle5Compatible){
                        mBle5.setSignalStrengthBLE5(SIGNAL_STRENGTH);
                    }
                }
                device_signalStrengthLevel.setText("Signal Strength: " + SIGNAL_STRENGTH + "dBm");
            }
        });
    }

    //Initiates the gui
    private void findViewByIdes() {
        onButton = (Button) findViewById(R.id.onButton);
        offButton = (Button) findViewById(R.id.offButton);
        signalStrength_increase = (Button) findViewById(R.id.device_signalStrength_increase);
        signalStrength_Decrease = (Button) findViewById(R.id.device_signalStrength_decrease);
        signalInterval_Increase = (Button) findViewById(R.id.device_intervall_increase);
        signalInterval_Decrease = (Button) findViewById(R.id.device_intervall_decrease);
        device_status = (TextView) findViewById(R.id.device_Status);
        device_data = (TextView) findViewById(R.id.device_Data);
        device_settings = (TextView) findViewById(R.id.device_Settings);
        device_mode = (TextView) findViewById(R.id.device_Mode);
        device_signalStrengthLevel = (TextView) findViewById(R.id.signalStrength);
        device_signalIntervalLevel = (TextView) findViewById(R.id.signalInterval);
    }

    //Method for checking if we can use BLE 5
    private boolean checkBLE5Compatibility(){
        //Requires API level 26 (Android 8)
        if(mBleAdapter.isLe2MPhySupported() && mBleAdapter.isLeCodedPhySupported() && mBleAdapter.isLeExtendedAdvertisingSupported() && mBleAdapter.isLePeriodicAdvertisingSupported()){
            return true;
        }
        else{
            Toast.makeText( this, "BLE 5 is a no go", Toast.LENGTH_SHORT ).show();
            //device_status.setText("No BLE 5");
            return false;
        }
    }

    //Sets up the advertising
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void advertise() throws UnsupportedEncodingException {
        mBLE_advertiser = BluetoothAdapter.getDefaultAdapter().getBluetoothLeAdvertiser(); //Requires API level 21 (Android 5) or higher
        mAdvertiserSettings = new AdvertiseSettings.Builder()
                .setAdvertiseMode( AdvertiseSettings.ADVERTISE_MODE_LOW_LATENCY)
                .setTxPowerLevel( AdvertiseSettings.ADVERTISE_TX_POWER_HIGH )
                .setConnectable( false )
                .build();
        pUuid = new ParcelUuid(UUID.fromString(BLE_UUID));
        mBleAdvertiseData = new AdvertiseData.Builder()
                .setIncludeDeviceName( true )
                .setIncludeTxPowerLevel(true)
                .addServiceUuid( pUuid )
                //.addServiceData( pUuid, "Data".getBytes("utf-8" ) )
                .build();
    }


    //Method for BLE, not BLE 5
    AdvertiseCallback mBleAdvCallback = new AdvertiseCallback() {
        @Override
        public void onStartSuccess(AdvertiseSettings settingsInEffect) {
            device_status.setText("Advertising");
            device_data.setText(mBleAdvertiseData.toString());
            device_settings.setText(mAdvertiserSettings.toString());
            super.onStartSuccess(settingsInEffect);
        }

        @Override
        public void onStartFailure(int errorCode) {
            StringBuilder errorMsg = new StringBuilder();
            errorMsg.append(errorCode);
            device_status.setText("Error: " + errorMsg);
            device_data.setText(mBleAdvertiseData.toString());
            device_settings.setText(mAdvertiserSettings.toString());
            super.onStartFailure(errorCode);
        }
    };

    //Sets or changes the signal interval
    public void setSignalInterval(int signalInterval){
        switch (signalInterval){
            case 1:
                SIGNAL_INTERVAL = 160;
                device_signalIntervalLevel.setText("Signal Interval: LOW");
                break;
            case 2:
                SIGNAL_INTERVAL = 400;
                device_signalIntervalLevel.setText("Signal Interval: MEDIUM");
                break;
            case 3:
                SIGNAL_INTERVAL = 1600;
                device_signalIntervalLevel.setText("Signal Interval: HIGH");
                break;
            case 4:
                SIGNAL_INTERVAL = 16777215;
                device_signalIntervalLevel.setText("Signal Interval: MAX");
                break;
        }
    }
}